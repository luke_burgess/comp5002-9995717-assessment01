﻿using System;

namespace comp5002_9995717_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {
            //Display a message that welcomes the user to the program
            Console.WriteLine("*************************************");
            Console.WriteLine("*************************************");
            Console.WriteLine("******** Welcome to my shop! ********");
            Console.WriteLine("*************************************");
            Console.WriteLine("*************************************");
           
            //Declare all variables used in the program
            string name = "";
            double num1 = 0;
            double num2 = 0;
            string answer = "";

            //Ask the user for their name
            Console.WriteLine("Please enter your name:");
            name = Console.ReadLine();
            

            //Ask the user for a number with 2 decimal places
            Console.Clear();
            Console.WriteLine($"Hello {name}! Please enter a number with two decimal places:");
            num1 = double.Parse(Console.ReadLine());

            //Display the user their number and ask them if they'd like to add another
            Console.Clear();
            Console.WriteLine($"Your current total is {num1:C2}");
            Console.WriteLine("Would you like to add a second number to your total?");
            Console.WriteLine("Y/N");
            answer = Console.ReadLine().ToLower();

            //If the user answers yes, allow them to add another number and add it to the total
            if(answer == "y")
            {
                Console.WriteLine("Please enter another number:");
                num2 = double.Parse(Console.ReadLine());
                num1 += num2;
            }
            //If the user answers no, display a message and prompt them to continue with a key press
            else
            {
                Console.WriteLine("No problem! Please press <Enter> to continue to checkout.");
                Console.ReadKey();  
            }

            //Display the total with and without added GST
            Console.Clear();
            Console.WriteLine($"Your total (ex. GST) is {num1:C2}");
            num1 = num1 * 1.15;
            Console.WriteLine($"Your total (incl. GST) is {num1:C2}");

            //Display a message thanking the user for using the program
            Console.WriteLine($"Thank you for shopping, {name}!");

            //Prompt the user to press a key to exit
            Console.WriteLine("Please press <Esc> to exit.");
            Console.ReadKey();
            Console.Clear();
        }
    }
}
